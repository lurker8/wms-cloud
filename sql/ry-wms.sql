/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : ry-wms

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 16/02/2023 16:54:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wms_account
-- ----------------------------
DROP TABLE IF EXISTS `wms_account`;
CREATE TABLE `wms_account`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账户编号',
  `account_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账户名称',
  `current_balance` decimal(10, 6) NULL DEFAULT NULL COMMENT '当前余额',
  `first_get` decimal(10, 6) NULL DEFAULT NULL COMMENT '期初余额',
  `account_type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账户类型',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_address
-- ----------------------------
DROP TABLE IF EXISTS `wms_address`;
CREATE TABLE `wms_address`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地址简称',
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系电话',
  `contacts` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人',
  `postal_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮政编码',
  `is_default` bit(1) NULL DEFAULT NULL COMMENT '是否默认',
  `province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '市',
  `area` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区',
  `address_detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详细地址',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '发货地址信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_check_inventory
-- ----------------------------
DROP TABLE IF EXISTS `wms_check_inventory`;
CREATE TABLE `wms_check_inventory`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '盘点单号',
  `status` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态:0-待提交,1-待审批,2-已批准',
  `worker_id` int(0) NULL DEFAULT NULL COMMENT '盘点人id',
  `warehouse_id` bigint(0) NULL DEFAULT NULL COMMENT '仓库id',
  `file_path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传盘点文件路径',
  `approval_time` datetime(0) NULL DEFAULT NULL COMMENT '审批时间',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '盘点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_check_inventory_item
-- ----------------------------
DROP TABLE IF EXISTS `wms_check_inventory_item`;
CREATE TABLE `wms_check_inventory_item`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `check_inventory_id` int(0) NULL DEFAULT NULL COMMENT '盘点单id',
  `sku_id` int(0) NULL DEFAULT NULL COMMENT '商品sku',
  `inventory_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库存变动类型(4-盘盈入库,5-盘亏出库)',
  `warehouse_id` int(0) NULL DEFAULT NULL COMMENT '仓库id',
  `qty` decimal(20, 6) NULL DEFAULT NULL COMMENT '盘点库存数量',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '盘点明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_customer
-- ----------------------------
DROP TABLE IF EXISTS `wms_customer`;
CREATE TABLE `wms_customer`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户编号',
  `customer_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户名称',
  `customer_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户类别',
  `customer_level` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户等级',
  `balance_date` datetime(0) NULL DEFAULT NULL COMMENT '余额日期',
  `first_get` decimal(20, 6) NULL DEFAULT NULL COMMENT '期初应收',
  `first_pre_get` decimal(20, 6) NULL DEFAULT NULL COMMENT '期初预收',
  `tax_identity` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纳税人识别号',
  `bank_info` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开户银行',
  `bank_num` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行账号',
  `seller_id` int(0) NULL DEFAULT NULL COMMENT '销售人员id',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_customer_contacts
-- ----------------------------
DROP TABLE IF EXISTS `wms_customer_contacts`;
CREATE TABLE `wms_customer_contacts`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人姓名',
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人手机',
  `tel` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐机',
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱/QQ/微信',
  `address` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `is_default` bit(1) NULL DEFAULT NULL COMMENT '是否默认',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `customer_id` int(0) NULL DEFAULT NULL COMMENT '客户ID',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户联系人信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_inventory
-- ----------------------------
DROP TABLE IF EXISTS `wms_inventory`;
CREATE TABLE `wms_inventory`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sku_id` bigint(0) NULL DEFAULT NULL COMMENT 'skuId',
  `warehouse_id` bigint(0) NULL DEFAULT NULL COMMENT '仓库id',
  `qty` decimal(20, 6) NULL DEFAULT NULL COMMENT '库存数量',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `version` int(0) NULL DEFAULT NULL COMMENT '版本号',
  `product_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_inventory_log
-- ----------------------------
DROP TABLE IF EXISTS `wms_inventory_log`;
CREATE TABLE `wms_inventory_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单号',
  `warehouse_id` int(0) NULL DEFAULT NULL COMMENT '仓库id',
  `inventory_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库存变动类型(0-采购入库,1-销售出库,2-调拨入库,3-调拨出库,4-盘盈入库,5-盘亏出库)',
  `sku_id` int(0) NULL DEFAULT NULL COMMENT '商品skuId',
  `price` decimal(20, 6) NULL DEFAULT NULL COMMENT '价格',
  `qty` decimal(20, 6) NULL DEFAULT NULL COMMENT '库存数量',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_product
-- ----------------------------
DROP TABLE IF EXISTS `wms_product`;
CREATE TABLE `wms_product`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品编号',
  `product_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品编码',
  `category_id` int(0) NULL DEFAULT NULL COMMENT '商品类别id',
  `is_spec` bit(1) NULL DEFAULT NULL COMMENT '是否多规格',
  `spec_list` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格参数',
  `product_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `unit` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品单位',
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图片',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `banner_images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `pro_sale_price` decimal(10, 2) NULL DEFAULT NULL,
  `pro_market_price` decimal(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_product_category
-- ----------------------------
DROP TABLE IF EXISTS `wms_product_category`;
CREATE TABLE `wms_product_category`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `parent_id` int(0) NULL DEFAULT NULL COMMENT '父类别id',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `sort` int(0) NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_product_sku
-- ----------------------------
DROP TABLE IF EXISTS `wms_product_sku`;
CREATE TABLE `wms_product_sku`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `bar_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品条码',
  `sku_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品sku名称',
  `specifications` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格',
  `product_id` int(0) NULL DEFAULT NULL COMMENT '商品id',
  `sale_price` decimal(10, 6) NULL DEFAULT NULL COMMENT '零售价',
  `trade_price` decimal(10, 6) NULL DEFAULT NULL COMMENT '批发价',
  `vip_price` decimal(10, 6) NULL DEFAULT NULL COMMENT '会员价',
  `discount1` decimal(10, 6) NULL DEFAULT NULL COMMENT '折扣一',
  `discount2` decimal(10, 6) NULL DEFAULT NULL COMMENT '折扣二',
  `purchase_price` decimal(10, 6) NULL DEFAULT NULL COMMENT '预计采购价',
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图片',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `market_price` decimal(10, 6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 343 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品sku信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_product_spec
-- ----------------------------
DROP TABLE IF EXISTS `wms_product_spec`;
CREATE TABLE `wms_product_spec`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `spec_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规格名称',
  `spec_attr` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规格属性',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品规格表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_purchase_order
-- ----------------------------
DROP TABLE IF EXISTS `wms_purchase_order`;
CREATE TABLE `wms_purchase_order`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '采购订单编号',
  `supplier_id` int(0) NULL DEFAULT NULL COMMENT '供应商id',
  `buy_date` datetime(0) NULL DEFAULT NULL COMMENT '购货日期',
  `bill_date` datetime(0) NULL DEFAULT NULL COMMENT '单据日期',
  `inventory_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '0-采购入库,6-采购退货',
  `inventory_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '0-未确认,1-确认入库,2-确认出库',
  `discount_rate` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠金额',
  `total_amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '合计金额',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `version` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_purchase_order_item
-- ----------------------------
DROP TABLE IF EXISTS `wms_purchase_order_item`;
CREATE TABLE `wms_purchase_order_item`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `purchase_order_id` int(0) NULL DEFAULT NULL COMMENT '订单主表id',
  `product_id` bigint(0) NULL DEFAULT NULL COMMENT '商品id',
  `sku_id` bigint(0) NULL DEFAULT NULL COMMENT 'skuId',
  `warehouse_id` int(0) NULL DEFAULT NULL COMMENT '仓库id',
  `price` decimal(20, 6) NULL DEFAULT NULL COMMENT '购买单价',
  `qty` decimal(20, 6) NULL DEFAULT NULL COMMENT '购买数量',
  `discount_rate` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠金额',
  `amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '金额',
  `memo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购订单明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_sale_order
-- ----------------------------
DROP TABLE IF EXISTS `wms_sale_order`;
CREATE TABLE `wms_sale_order`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '销售单编号',
  `customer_id` int(0) NULL DEFAULT NULL COMMENT '客户id',
  `worker_id` int(0) NULL DEFAULT NULL COMMENT '销售人id',
  `bill_date` datetime(0) NULL DEFAULT NULL COMMENT '单据日期',
  `address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户地址',
  `inventory_status` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '0-未确认,1-确认入库,2-确认出库',
  `inventory_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '1-销售出库',
  `discount_rate` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠金额',
  `total_amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '合计金额',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `version` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_sale_order_item
-- ----------------------------
DROP TABLE IF EXISTS `wms_sale_order_item`;
CREATE TABLE `wms_sale_order_item`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sale_order_id` int(0) NULL DEFAULT NULL COMMENT '销售订单主表id',
  `product_id` int(0) NULL DEFAULT NULL COMMENT '商品id',
  `sku_id` int(0) NULL DEFAULT NULL COMMENT '商品sku_id',
  `warehouse_id` int(0) NULL DEFAULT NULL COMMENT '仓库id',
  `price` decimal(20, 6) NULL DEFAULT NULL COMMENT '销售单价',
  `sale_qty` decimal(20, 6) NULL DEFAULT NULL COMMENT '销售数量',
  `discount_rate` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠率',
  `discount_amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '优惠金额',
  `amount` decimal(20, 6) NULL DEFAULT NULL COMMENT '金额',
  `memo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售订单明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_supplier
-- ----------------------------
DROP TABLE IF EXISTS `wms_supplier`;
CREATE TABLE `wms_supplier`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '供应商编号',
  `supplier_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户名称',
  `supplier_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户类别',
  `balance_date` datetime(0) NULL DEFAULT NULL COMMENT '余额日期',
  `first_pay` decimal(20, 6) NULL DEFAULT NULL COMMENT '期初应付',
  `first_pre_pay` decimal(20, 6) NULL DEFAULT NULL COMMENT '期初预付款',
  `tax_identity` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纳税人识别号',
  `tax_rate` decimal(20, 6) NULL DEFAULT NULL COMMENT '增值税税率',
  `bank_info` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开户银行',
  `bank_num` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行账号',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_supplier_contacts
-- ----------------------------
DROP TABLE IF EXISTS `wms_supplier_contacts`;
CREATE TABLE `wms_supplier_contacts`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人姓名',
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人手机',
  `tel` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐机',
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱/QQ/微信',
  `address` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `is_default` bit(1) NULL DEFAULT NULL COMMENT '是否默认',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `supplier_id` int(0) NULL DEFAULT NULL COMMENT '客户ID',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商联系人信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_transfer_order
-- ----------------------------
DROP TABLE IF EXISTS `wms_transfer_order`;
CREATE TABLE `wms_transfer_order`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调拨单编号',
  `bill_date` datetime(0) NULL DEFAULT NULL COMMENT '单据日期',
  `memo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调拨单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_transfer_order_item
-- ----------------------------
DROP TABLE IF EXISTS `wms_transfer_order_item`;
CREATE TABLE `wms_transfer_order_item`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `transfer_order_id` int(0) NULL DEFAULT NULL COMMENT '调拨单id',
  `warehouse_in_id` int(0) NULL DEFAULT NULL COMMENT '入库仓',
  `warehouse_out_id` int(0) NULL DEFAULT NULL COMMENT '出库仓',
  `sku_id` int(0) NULL DEFAULT NULL COMMENT '商品sku',
  `qty` decimal(20, 6) NULL DEFAULT NULL COMMENT '调拨数量',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调拨单明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `wms_warehouse`;
CREATE TABLE `wms_warehouse`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '仓库编号',
  `warehouse_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '仓库名称',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wms_worker
-- ----------------------------
DROP TABLE IF EXISTS `wms_worker`;
CREATE TABLE `wms_worker`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职员编号',
  `worker_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职员名称',
  `phone` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标志（0代表存在 1代表删除）',
  `tenant_id` bigint(0) NULL DEFAULT NULL COMMENT '租户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '职员信息表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
