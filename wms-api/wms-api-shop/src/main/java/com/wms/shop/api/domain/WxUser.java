package com.wms.shop.api.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 微信小程序用户
 */
@Data
@TableName("shop_user")
public class WxUser implements Serializable {

    /** ID */
    private Long id;

    /** 微信openid */
    private String openid;

    /** 用户名称 */
    private String userName;

    /** 用户手机号 */
    private String phone;

    /** 租户id */
    @TableField(exist = false)
    private Long tenantId;

    /**
     * 会话密钥
     */
    @TableField(exist = false)
    private String sessionKey;
}
