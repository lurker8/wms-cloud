package com.wms.shop.api.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zzm
 * @date 2022/11/11
 * @desc
 */
@Data
public class Cart implements Serializable {
}
