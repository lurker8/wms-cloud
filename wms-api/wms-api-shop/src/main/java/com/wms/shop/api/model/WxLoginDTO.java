package com.wms.shop.api.model;

import lombok.Data;

@Data
public class WxLoginDTO {


    private String jsCode;


    private String appId;


    private Long tenantId;
}
