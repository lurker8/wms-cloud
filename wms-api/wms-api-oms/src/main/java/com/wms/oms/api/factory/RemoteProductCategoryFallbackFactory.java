package com.wms.oms.api.factory;

import com.wms.common.core.domain.R;
import com.wms.oms.api.RemoteProductCategoryService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class RemoteProductCategoryFallbackFactory implements FallbackFactory<RemoteProductCategoryService> {
    @Override
    public RemoteProductCategoryService create(Throwable throwable) {
        
        return new RemoteProductCategoryService() {
            @Override
            public R<List> getProductCategoryList() {
                return null;
            }
        };
    }
}
