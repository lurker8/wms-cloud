package com.wms.oms.api.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductCategoryDTO implements Serializable {


    /** ID */
    private Long id;

    /** 分类名称 */
    private String categoryName;

    /** 分类图标 */
    private String icon;
}
