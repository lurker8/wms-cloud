package com.wms.oms.domain.dto;

import com.wms.common.core.domain.BannerImage;
import com.wms.oms.domain.ProductCategory;
import com.wms.oms.domain.ProductSku;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 微信小程序 商品dto
 */

@Data
public class WxProductDto implements Serializable {

    /** ID */
    private Long id;

    /** 商品编号 */
    private String sn;

    /** 商品编码 */
    private String productCode;

    /**
     * 上传园区banner图
     */
    private List<BannerImage> bannerImages;

    /** 商品类别id */
    private Long categoryId;

    /** 是否多规格 */
    private Boolean isSpec;

    /** 商品名称  */
    private String productName;

    /** 商品单位  */
    private String unit;

    /** 规格参数 */
    private List<SpecDto> specDtoList;


    /** 租户ID */
    private Long tenantId;

    /** 关联分类 */
    private ProductCategory productCategory;

    /** 关联sku */
    private List<ProductSku> productSkuList;

    // 显示sku最低销售价
    private BigDecimal proSalePrice;

    // 显示sku最低市场价
    private BigDecimal proMarketPrice;

    // 库存数量
    private BigDecimal qty;

    /** 备注 */
    private String remark;
}
