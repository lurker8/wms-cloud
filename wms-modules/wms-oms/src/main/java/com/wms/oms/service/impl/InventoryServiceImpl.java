package com.wms.oms.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import com.wms.common.core.utils.DateUtils;
import com.wms.common.core.utils.StringUtils;
import com.wms.oms.annotation.InventoryLock;
import com.wms.oms.annotation.LogOms;
import com.wms.oms.domain.InventoryLog;
import com.wms.oms.domain.PurchaseOrder;
import com.wms.oms.domain.PurchaseOrderItem;
import com.wms.oms.mapper.InventoryLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wms.common.core.utils.SecurityUtils;
import com.wms.oms.mapper.InventoryMapper;
import com.wms.oms.domain.Inventory;
import com.wms.oms.service.IInventoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.util.CollectionUtils;

/**
 * 库存信息Service业务层处理
 *
 * @author zzm
 * @date 2021-05-16
 */
@Service
public class InventoryServiceImpl implements IInventoryService
{
    @Autowired
    private InventoryMapper inventoryMapper;


    @Autowired
    private InventoryLogMapper inventoryLogMapper;

    /**
     * 查询库存信息
     *
     * @param id 库存信息ID
     * @return 库存信息
     */
    @Override
    public Inventory selectInventoryById(Long id)
    {
        return inventoryMapper.selectById(id);
    }

    /**
     * 判断库存是否存在
     * @param skuId
     * @param warehouseId
     * @return
     */
    @Override
    public Inventory selectInventoryByPro(Long skuId, Long warehouseId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("sku_id", skuId);
        queryWrapper.eq("warehouse_id", warehouseId);
        return inventoryMapper.selectOne(queryWrapper);
    }

    /**
     * 查询库存信息列表
     *
     * @param inventory 库存信息
     * @return 库存信息
     */
    @Override
    public List<Inventory> selectInventoryList(Inventory inventory)
    {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (!CollectionUtils.isEmpty(inventory.getParams())) {
            if (inventory.getParams().get("productName") != null) {
                queryWrapper.like("wp.product_name", inventory.getParams().get("productName"));
            }
            if (inventory.getParams().get("warehouseName") != null){
                queryWrapper.like("ww.warehouse_name", inventory.getParams().get("warehouseName"));
            }
        }
        queryWrapper.orderByDesc("wi.id");
        return inventoryMapper.selectList(queryWrapper);
    }

    /**
     * 新增库存信息
     *
     * @param inventory 库存信息
     * @return 结果
     */
    @Override
    public int insertInventory(Inventory inventory)
    {
        inventory.setDelFlag(false);
        inventory.setCreateBy(SecurityUtils.getUsername());
        inventory.setCreateTime(DateUtils.getNowDate());
        return inventoryMapper.insert(inventory);
    }

    /**
     * 修改库存信息
     *
     * @param inventory 库存信息
     * @return 结果
     */
    @Override
    public int updateInventory(Inventory inventory)
    {
        inventory.setUpdateBy(SecurityUtils.getUsername());
        inventory.setUpdateTime(DateUtils.getNowDate());
        return inventoryMapper.updateById(inventory);
    }

    /**
     * 批量删除库存信息
     *
     * @param ids 需要删除的库存信息ID
     * @return 结果
     */
    @Override
    public int deleteInventoryByIds(Long[] ids)
    {
        Inventory inventory = new Inventory();
        inventory.setDelFlag(Boolean.TRUE);
        inventory.setUpdateTime(DateUtils.getNowDate());
        inventory.setUpdateBy(SecurityUtils.getUsername());
        QueryWrapper<Inventory> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",Arrays.asList(ids));
        return inventoryMapper.update(inventory, queryWrapper);
    }

    /**
     * 删除库存信息信息
     *
     * @param id 库存信息ID
     * @return 结果
     */
    @Override
    public int deleteInventoryById(Long id)
    {
        return inventoryMapper.deleteById(id);
    }


    @Override
    @InventoryLock(skuId = "#purchaseOrderItem.skuId", warehouseId = "#purchaseOrderItem.warehouseId")
    public void putInInventory(PurchaseOrder purchaseOrder, PurchaseOrderItem purchaseOrderItem) {
        Inventory inventory =  this.selectInventoryByPro(purchaseOrderItem.getSkuId(), purchaseOrderItem.getWarehouseId());
        if(inventory == null){
            // 记录新库存
            inventory = new Inventory();
            inventory.setProductId(purchaseOrderItem.getProductId());
            inventory.setSkuId(purchaseOrderItem.getSkuId());
            inventory.setQty(purchaseOrderItem.getQty());
            inventory.setWarehouseId(purchaseOrderItem.getWarehouseId());
            inventory.setDelFlag(Boolean.FALSE);
            inventory.setCreateBy(SecurityUtils.getUsername());
            inventory.setCreateTime(DateUtils.getNowDate());
            this.insertInventory(inventory);
        }else{
            inventory.setQty(inventory.getQty()+ purchaseOrderItem.getQty());
            this.updateInventory(inventory);
        }

        // 库存日志
        InventoryLog inventoryLog = new InventoryLog();
        inventoryLog.setInventoryType(purchaseOrder.getInventoryType());
        inventoryLog.setSkuId(purchaseOrderItem.getSkuId());
        inventoryLog.setDelFlag(Boolean.FALSE);
        inventoryLog.setWarehouseId(purchaseOrderItem.getWarehouseId());
        inventoryLog.setQty(purchaseOrderItem.getQty());
        inventoryLog.setPrice(purchaseOrderItem.getPrice());
        inventoryLog.setSn(purchaseOrder.getSn());
        inventoryLog.setCreateTime(inventory.getCreateTime());
        inventoryLog.setCreateBy(SecurityUtils.getUsername());
        inventoryLogMapper.insert(inventoryLog);
    }
}