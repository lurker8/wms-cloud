package com.wms.oms.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zzm
 * @date 2022/12/21
 * @desc 库存锁注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface InventoryLock {

    String skuId() default "";

    String warehouseId() default "";
}
