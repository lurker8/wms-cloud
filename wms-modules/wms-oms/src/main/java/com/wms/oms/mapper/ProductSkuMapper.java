package com.wms.oms.mapper;

import com.wms.oms.domain.ProductSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品sku信息Mapper接口
 *
 * @author zzm
 * @date 2021-05-15
 */
public interface ProductSkuMapper extends BaseMapper<ProductSku>
{

    /**
     * 根据productId 查询最低销售价和市场价
     * @param productId
     * @return
     */
    ProductSku selectMinResult(Long productId);

}