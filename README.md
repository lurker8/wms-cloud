## 平台简介

基于若依微服务源码做一个集成docker脚本的小demo,原代码结构不变，只添加oms功能模块。

* 采用前后端分离的模式，微服务版本前端(基于 [RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue))。
* 后端采用Spring Boot、Spring Cloud & Alibaba。
* 注册中心、配置中心选型Nacos，权限认证使用Redis。
* 流量控制框架选型Sentinel，分布式事务选型Seata。


#### 友情链接 [若依/RuoYi-Cloud](https://gitee.com/zhangmrit/ruoyi-cloud) Ant Design版本。


## 架构图

<img src="https://oscimg.oschina.net/oscnet/up-82e9722ecb846786405a904bafcf19f73f3.png"/>

## ruoyi内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 新增wms-oms模块 

1.  商品管理：商品类型，规格，详情等设置。
2.  采购管理：采购单录入。
3.  销售管理：销售单录入。
4.  库存管理：库存查询、库存日志



##代码改动：
1.  添加mybatis-plus,多租户简单实现。
2.  添加dockerfile,镜像打包。
3.  添加docker-compose脚本，集成yapi。
4.  添加junit测试。
5.  改动了数据库脚本。
6.  前端:https://gitee.com/lindazi/wms-ui
7.  集成WxJava微信登录功能。

nacos指令：startup  -m standalone 
本地运行先设置hosts<br>

127.0.0.1   nacos<br>
127.0.0.1   mysql<br>
127.0.0.1   redis<br>
127.0.0.1   wms-auth<br>
127.0.0.1   wms-gateway<br>
127.0.0.1   wms-system<br>
127.0.0.1   wms-oms<br>
127.0.0.1   wms-file<br>
127.0.0.1   wms-gen<br>
127.0.0.1   wms-job<br>

## 后端演示图


<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/053626_2d0588b2_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/053725_a6b76baa_9060750.png"/></td>
    </tr>
    <tr>
       <td><img src="https://images.gitee.com/uploads/images/2021/0811/053840_ac888d22_9060750.png"/></td>
       <td><img src="https://images.gitee.com/uploads/images/2021/0811/053908_bd4a1950_9060750.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/053934_68cacb1c_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/053956_a43a41f9_9060750.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054027_200cc18b_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054110_749468d6_9060750.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054134_13fbe299_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054202_fcccafec_9060750.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054311_27e86f2f_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0824/061815_2611ea14_9060750.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054436_d234febe_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054500_bcccee3f_9060750.png"/></td>
    </tr>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054528_89af6d15_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054601_6682d626_9060750.png"/></td>
    </tr>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054640_4498a3a4_9060750.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0811/054701_f64ccc6d_9060750.png"/></td>
    </tr>
</table>

## 小程序演示图
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/1121/211311_d7ba26a4_9060750.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/1121/211347_0cd1eb32_9060750.jpeg "微信图片_22.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/1121/211407_6aa7e21e_9060750.jpeg"/></td>
    </tr>
</table>

测试数据库超管账号/密码:admin/admin123

演示地址:https://d3m2561525.goho.co
演示账号/密码:19088888888/123456
体验版小程序，需要查看可私信我。
![小程序体验版](oG0tS5fJDK5mOq0l1iRLaKQsW-lE%20(2).jpg)
