package com.wms.auth.service;

import com.wms.common.core.constant.Constants;
import com.wms.common.core.domain.R;
import com.wms.common.core.exception.BaseException;
import com.wms.shop.api.RemoteWxUserService;
import com.wms.shop.api.model.WxLoginDTO;
import com.wms.shop.api.model.WxLoginUser;
import com.wms.system.api.RemoteLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WxLoginService {


    @Autowired
    private RemoteLogService remoteLogService;


    @Autowired
    private RemoteWxUserService remoteWxUserService;
    /**
     * 登录
     */
    public WxLoginUser wxLogin(WxLoginDTO wxLoginDTO)
    {
        // 查询用户信息
        R<WxLoginUser> userResult = remoteWxUserService.getWxUserInfo(wxLoginDTO);
        if (R.FAIL == userResult.getCode())
        {
            throw new BaseException(userResult.getMsg());
        }
        WxLoginUser userInfo = userResult.getData();
        remoteLogService.saveLogininfor(userInfo.getWxUser().getOpenid(), Constants.LOGIN_SUCCESS, "登录成功");
        return userInfo;
    }
}
